tool
extends Control

func searchPage(page):
	var search_term = get_node("VBoxContainer/SearchTermBox").get_text()
	openPage(page + search_term.percent_encode())

func openPage(page):
	if OS.get_name() == "OSX":
		OS.execute("open", [page], true)
	elif OS.get_name() == "X11":
		OS.execute("xdg-open", [page], true)
	else:
		OS.execute("cmd", ["/c start " + page], true)

func _on_OpenGameArtButton_pressed():
	searchPage("https://opengameart.org/art-search?keys=")

func _on_KenneyButton_pressed():
	searchPage("https://kenney.nl/assets?s=")

func _on_BlendswapButton_pressed():
	searchPage("https://www.blendswap.com/blends/search?keywords=")

func _on_Free3dButton_pressed():
	searchPage("https://free3d.com/search/?search=Search&q=")

func _on_SketchFabButton_pressed():
	searchPage("https://sketchfab.com/search?features=downloadable&sort_by=-pertinence&type=models&q=")

func _on_ItchIoButton_pressed():
	openPage("https://itch.io/game-assets/free")

func _on_TexturesButton_pressed():
	searchPage("https://www.textures.com/search?q=")

func _on_FreePBRButton_pressed():
	openPage("https://freepbr.com/")

func _on_DevAssetsButton_pressed():
	openPage("http://devassets.com/")

func _on_OpenPixelProjectButton_pressed():
	openPage("http://www.openpixelproject.com/")
